//khai báo thư viện express Js
const express = require("express");
//khai báo router chạy
const router = express.Router();

//khai báo controller user
const userController = require ("../controllers/userController");

router.post ("/users", userController.createUser);
router.get("/users", userController.getAllUser);
router.get("/users/:userId", userController.getUserById);
router.put("/users/:userId", userController.updateUserById);
router.delete("/users/:userId", userController.deleteUserById);
module.exports = router;